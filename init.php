<?php
class Af_Youtube_Thumb extends Plugin {

	function about() {
		return array(null,
			"Show Youtube videos as clickable thumbnails",
			"fox");
	}

	function init($host) {
		$host->add_hook($host::HOOK_RENDER_ENCLOSURE, $this);
		$host->add_hook($host::HOOK_RENDER_ARTICLE, $this);
		$host->add_hook($host::HOOK_RENDER_ARTICLE_CDM, $this);
		$host->add_hook($host::HOOK_RENDER_ARTICLE_API, $this);
		$host->add_hook($host::HOOK_ARTICLE_IMAGE, $this);
	}

	function get_css() {
		return file_get_contents(__DIR__ . "/init.css");
	}

	function hook_article_image($enclosures, $content, $site_url, $article) {

		/** @var array<string,string> $enc */
		foreach ($enclosures as $enc) {
			if ($vid_id = UrlHelper::url_to_youtube_vid($enc["content_url"])) {
				$thumb_url = htmlspecialchars("https://img.youtube.com/vi/$vid_id/hqdefault.jpg");

				return [$thumb_url, "", $content];
			}
		}

		// TODO: deal with youtube URLs in article content

		return ["", "", $content];
	}

	function hook_render_enclosure($entry, $article_id, $rv) {

		$url = $entry["content_url"];

		if ($vid_id = UrlHelper::url_to_youtube_vid($url)) {

			$thumb_url = htmlspecialchars("https://img.youtube.com/vi/$vid_id/hqdefault.jpg");
			$url = htmlspecialchars($url);

			return "<a target='_blank' rel='noopener noreferrer' href=\"$url\" title=\"".$this->__("Click to open video")."\">
				<div class='youtube-thumb'>
					<img class='thumbnail' src=\"$thumb_url\" referrerpolicy='no-referrer'>
					<div class='watermark'></div>
				</div>
			</a>";
		}

		return "";
	}

	function hook_render_article($article) {
		return $this->hook_render_article_cdm($article);
	}

	function hook_render_article_api($params) {
		$article = isset($params['headline']) ? $params['headline'] : $params['article'];

		return $this->hook_render_article_cdm($article);
	}

	/* function hook_render_article_api($row) {
		$article = isset($row['headline']) ? $row['headline'] : $row['article'];

		return $this->hook_render_article_cdm($article, true);
	} */

	function hook_render_article_cdm($article) {
		$doc = new DOMDocument();
		$need_saving = false;

		if (!empty($article["content"]) && @$doc->loadHTML($article["content"])) {
			$xpath = new DOMXPath($doc);
			$iframes = $xpath->query("//iframe[@src]");

			foreach ($iframes as $iframe) {
				$url = $iframe->getAttribute("src");

				if ($vid_id = UrlHelper::url_to_youtube_vid($url)) {
					$thumb_url = htmlspecialchars("https://img.youtube.com/vi/$vid_id/hqdefault.jpg");

					$img = $doc->createElement("img");
					$img->setAttribute("src", $thumb_url);

					$watermark = $doc->createElement("div");
					$watermark->setAttribute("class", "watermark");

					$div = $doc->createElement("div");
					$div->setAttribute("class", "youtube-thumb");

					$div->appendChild($img);
					$div->appendChild($watermark);

					$a = $doc->createElement("a");
					$a->setAttribute("target", "_blank");
					$a->setAttribute("href", $url);

					$a->appendChild($div);

					$parent = $iframe->parentNode;

					if ($parent->getAttribute("class") == "embed-responsive")
						$parent->setAttribute("class", "");

					$parent->replaceChild($a, $iframe);

					$need_saving = true;
				}
			}
		}

		if ($need_saving) $article["content"] = $doc->saveXML();

		return $article;
	}


	function api_version() {
		return 2;
	}

}
